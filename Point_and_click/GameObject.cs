﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point_and_click
{
    class GameObject
    {
        public Rectangle clickBounds;
        public Rectangle drawBounds;
        public Rectangle blockBounds;
        public Texture2D texture;
        public Color tint;

        public GameObject(Rectangle? clickBounds,Rectangle? drawBounds, Rectangle? blockBounds,Texture2D texture,Color? tint)
        {
            //non nullable values
            if (clickBounds != null) { this.clickBounds = clickBounds.Value; }
            if (drawBounds != null) { this.drawBounds = drawBounds.Value; }
            if (blockBounds != null) { this.blockBounds = blockBounds.Value; }
            if (tint != null) { this.tint = tint.Value; }
            
            //nullable values
            this.texture = texture;
        }

        public static GameObject FromData(string[] Data)
        {
            GameObject Output = new GameObject(null, null, null, null, null);
            foreach (string value in Data)
            {
                string[] args = value.Split(' ');
                if (args.Length < 1)
                {
                    Game1.error("found empty data");
                    continue;
                }
                switch (args[0].ToLower())
                {
                    case "clickbounds":
                    case "click":
                        Output.clickBounds = (Rectangle)EvaluateObject(args.Skip(1).ToArray(),"rectangle");
                        break;
                    case "drawbounds":
                    case "draw":
                        Output.drawBounds = (Rectangle)EvaluateObject(args.Skip(1).ToArray(), "rectangle");
                        break;
                    case "blockbounds":
                    case "block":
                        Output.blockBounds = (Rectangle)EvaluateObject(args.Skip(1).ToArray(), "rectangle");
                        break;
                    case "tint":
                        Output.tint = (Color)EvaluateObject(args.Skip(1).ToArray(), "color");
                        break;
                    case "texture":
                        Output.texture = Game1.TextureFromFile(args.Skip(1).ToArray());
                        break;
                    default:
                        Game1.error("No value for '" + args[0].ToLower() + "' exists in gameobject");
                        break;
                }
            }


            return Output;
        }

        public static object EvaluateObject(string[] values,string type)
        {
            switch (type.ToLower())
            {
                case "rectangle":
                    if (values.Length < 4)
                    {
                        return null;
                    }
                    return new Rectangle(CastInt(values[0]), CastInt(values[1]), CastInt(values[2]), CastInt(values[3]));
                case "color":
                    if (values.Length < 4)
                    {
                        if (values.Length < 3)
                        {
                            return null;
                        }
                        return new Color(CastFloat(values[0]), CastFloat(values[1]), CastFloat(values[2]));
                    }
                    return new Color(CastFloat(values[0]), CastFloat(values[1]), CastFloat(values[2]), CastFloat(values[3]));
                default:
                    Game1.error("cannot evaluate unkown type: " + type.ToString() + " with values: " + string.Join(", ", values));
                    return null;
            }
        }

        public static int CastInt(string value)
        {
            int v;
            if (!int.TryParse(value,out v))
            {
                Game1.error("Could not parse int from: " + value);
                return 0;
            }
            return v;
        }

        public static float CastFloat(string value)
        {
            float v;
            if (!float.TryParse(value, out v))
            {
                Game1.error("Could not parse float from: " + value);
                return 0;
            }
            return v;
        }

        public void Draw(SpriteBatch s)
        {
            if (texture != null)
            {
                s.Draw(texture,new Rectangle(drawBounds.Location + Game1.Viewport.Location,drawBounds.Size) , tint != null ? tint : Color.White);
            }
        }
    }
}
