﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.IO;

namespace Point_and_click
{
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        internal static string ActiveDirectory;
        List<PointAndClickGame> Games;
        KeyboardState oldks;
        MouseState oldms;
        internal static GraphicsDevice GD;
        internal static Texture2D Pixel;

        public static Rectangle Viewport { private set;  get; }
        public static Rectangle screen { private set; get; }

        PointAndClickGame CurrentGame;

        internal static SpriteFont font;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }
        
        protected override void Initialize()
        {
            base.Initialize();
            ActiveDirectory = Directory.GetCurrentDirectory() + "\\";
            oldms = Mouse.GetState();
            oldks = Keyboard.GetState();

            screen = new Rectangle(0, 0, GraphicsDevice.DisplayMode.Width, GraphicsDevice.DisplayMode.Height);

            Viewport = new Rectangle(0, 0, 1920, 1080);
            if (screen.Width < Viewport.Width)
            {
                float scale = (float)screen.Width / Viewport.Width;
                Viewport = new Rectangle(Viewport.Location,new Point(screen.Width, (int)(Viewport.Height * scale)));
                Viewport = new Rectangle(new Point(Viewport.Location.X, (screen.Height - Viewport.Height) / 2),Viewport.Size);
            }

            graphics.PreferredBackBufferWidth = screen.Width;
            graphics.PreferredBackBufferHeight = screen.Height;
            graphics.IsFullScreen = false;
            IsMouseVisible = true;
            graphics.ApplyChanges();

            Pixel = new Texture2D(GraphicsDevice,1, 1);
            Pixel.SetData<Color>(new Color[] { Color.White });



            Games = FindGames(ActiveDirectory, Viewport);
        }
        
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            GD = GraphicsDevice;

            font = Content.Load<SpriteFont>("font");
        }
        
        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            KeyboardState ks = Keyboard.GetState();
            MouseState ms = Mouse.GetState();

            if (ks.IsKeyDown(Keys.Escape))
                Exit();

            if (ms.LeftButton == ButtonState.Pressed && oldms.LeftButton == ButtonState.Released)
            {
                if (CurrentGame != null)
                {
                    //CurrentGame.Click(ms.X, ms.Y, Left);
                }
                else
                {
                    foreach (PointAndClickGame g in Games)
                    {
                        if (g.DrawRegion.Contains(new Point(ms.X,ms.Y)))
                        {
                            g.Load();
                            CurrentGame = g;
                            break;
                        }
                    }
                }
            }

            oldks = ks;
            oldms = ms;
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            spriteBatch.Begin();
            spriteBatch.Draw(Pixel, Viewport, Color.CornflowerBlue);
            if (CurrentGame != null)
            {
                CurrentGame.Draw(spriteBatch);
            }
            else
            {
                DrawGames();
            }
            spriteBatch.End();
            base.Draw(gameTime);
        }

        internal void DrawGames()
        {
            foreach (PointAndClickGame g in Games)
            {
                g.DrawIcon(spriteBatch,oldms);
            }
        }

        public static void error(string e)
        {

        }

        static List<PointAndClickGame> FindGames(string filePath,Rectangle Viewport)
        {
            List<PointAndClickGame> games = new List<PointAndClickGame>();
            if (!Directory.Exists(filePath))
            {
                return games;
            }
            string[] Directories = Directory.GetDirectories(filePath);
            foreach (string s in Directories)
            {
                string[] files = Directory.GetFiles(s);
                foreach (string f in files)
                {
                    if (Path.GetExtension(f) == ".pncg")
                    {
                        games.Add(PointAndClickGame.FromFile(f));
                    }
                }
            }
            int width = (int)Math.Ceiling(Math.Sqrt(games.Count));
            Rectangle region = new Rectangle(Viewport.Location, Viewport.Size);
            region.Inflate(-100, -100);

            Point size = new Point(region.Width > region.Height ? region.Height / width : region.Width / width);
            for (int i = 0; i < games.Count; i++)
            {
                games[i].DrawRegion = new Rectangle(size * new Point(i % width, (int)i / width) + region.Location, size);
            }
            return games;
        }

        internal static Texture2D TextureFromFile(string filepath)
        {
            if (!File.Exists(filepath))
            {
                if (File.Exists(Game1.ActiveDirectory + filepath))
                {
                    filepath = ActiveDirectory + filepath;
                }
                else
                {
                    Game1.error("Texture does not exist '" + filepath + "'");
                    return null;
                }
            }
            if (new List<string> (new string[] { ".png" }).Contains(Path.GetExtension(filepath)))
            {
                using (FileStream fs = new FileStream(filepath,FileMode.Open))
                {
                    return Texture2D.FromStream(GD, fs);
                }
            }
            error("Could not load image with format '" + Path.GetExtension(filepath) + "'");
            return null;
        }
        internal static Texture2D TextureFromFile(string[] filepath)
        {
            if (filepath.Length < 1)
            {
                error("Tried to load texture withour filepath");
                return null;
            }
            return TextureFromFile(filepath[0]);
        }
    }
}
