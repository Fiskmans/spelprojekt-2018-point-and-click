﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point_and_click
{
    class Room
    {
        List<GameObject> objects;
        Rectangle Bounds;

        internal static Room FromFile(string filepath)
        {
            Room room = new Room();
            room.objects = new List<GameObject>();
            if (!File.Exists(filepath))
            {
                if (File.Exists(Game1.ActiveDirectory + filepath))
                {
                    filepath = Game1.ActiveDirectory + filepath;
                }
                else
                {
                    Game1.error("Room does not exist '" + filepath + "'");
                    return null;
                }
            }
            Dictionary<string, string[]> Data = new Dictionary<string, string[]>();
            string[] RawData = File.ReadAllLines(filepath);
            for (int i = 0; i < RawData.Length; i++)
            {
                List<string> args = new List<string>();
                int count = 0;
                while (RawData.Count() > i + count + 1 && RawData[i+count+1].Substring(0,1) == " ")
                {
                    count++;
                    args.Add(RawData[i + count].Substring(1));
                }
                Data.Add(RawData[i], args.ToArray());
                i += count;
            }
            foreach (KeyValuePair<string,string[]> kv in Data)
            {
                string[] args = kv.Key.Split(' ');
                if (args.Length > 0 && args[0].ToLower() == "object")
                {
                    if (args.Length > 1)
                    {
                        switch (args[1])
                        {
                            default:
                                break;
                        }
                    }
                    else
                    {
                        room.objects.Add(GameObject.FromData(kv.Value));
                    }
                }
                
            }

            return room;


        }
        internal static Room FromFile(string[] filepath)
        {
            if (filepath.Length < 1)
            {
                Game1.error("Tried to load non-existing room");
                return null;
            }
            return FromFile(filepath[0]);
        }

        internal void Draw(SpriteBatch s)
        {
            foreach (GameObject obj in objects)
            {
                obj.Draw(s);
            }
        }
    }
}
