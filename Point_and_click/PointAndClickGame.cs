﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point_and_click
{
    class PointAndClickGame
    {
        internal string name;
        internal Texture2D Logo;
        internal int iconSize;
        internal Rectangle DrawRegion;

        string filepath;

        Room startingRoom;

        Room CurrentRoom;

        public static PointAndClickGame FromFile(string filepath)
        {
            PointAndClickGame game = new PointAndClickGame();
            game.name = Path.GetFileNameWithoutExtension(filepath);
            game.Logo = Game1.TextureFromFile(Path.GetDirectoryName(filepath) + "\\logo.png");
            game.iconSize = -15;
            game.filepath = filepath;
            game.DrawRegion = new Rectangle(0, 0, 0, 0);

            return game;
        }


        public void Load()
        {
            Game1.ActiveDirectory = Path.GetDirectoryName(filepath) + "\\";
            if (File.Exists(filepath))
            {
                string[] RawData = File.ReadAllLines(filepath);
                foreach (string Data in RawData)
                {
                    string[] args = Data.Split(' ');
                    if (args.Length > 0)
                    {
                        switch (args[0].ToLower())
                        {
                            case "startingroom":
                            case "startroom":
                                startingRoom = Room.FromFile(args.Skip(1).ToArray());
                                break;
                            default:
                                Game1.error("Unkown attribute '" + args[0] + "' for game");
                                break;
                        }
                    }
                }
            }

            CurrentRoom = startingRoom;
        }

        public void DrawIcon(SpriteBatch s, MouseState ms)
        {
            Rectangle icon = new Rectangle(DrawRegion.Location, DrawRegion.Size);
            icon.Inflate(iconSize, iconSize);
            if (DrawRegion.Contains(new Point(ms.X, ms.Y)))
            {
                s.Draw(Game1.Pixel, icon, Color.DarkGreen);
                iconSize += iconSize < 0 ? 3 : 0;
            }
            else
            {
                iconSize -= iconSize > -15 ? 3 : 0;
            }
            s.Draw(Logo, icon, Color.White);
            s.DrawString(Game1.font, name, new Vector2((DrawRegion.Left + DrawRegion.Right) / 2, DrawRegion.Bottom) - new Vector2(Game1.font.MeasureString(name).X / 2, Game1.font.MeasureString(name).Y), Color.Black);
        }

        internal void Draw(SpriteBatch s)
        {
            if (CurrentRoom != null)
            {
                CurrentRoom.Draw(s);
            }
        }

    }
}
